[**This project has been permanently moved to github**](https://github.com/Gunneone/Telegraphs) 

---

**These Jupyter Notebooks are meant to give you some nice statistics about your Telegram Usage.**

---

## To get started:
1. Install Python and Jupyter Notebook. If you haven't installed any of these yet, I recommend [Anaconda](https://www.anaconda.com/products/individual).
2. [Export your Telegram History](https://telegram.org/blog/export-and-more) as json via the [Telegram Desktop Client](https://desktop.telegram.org/).
3. Place the exported file 'result.json' in the notebooks folder.
4. Open Jupyter Notebook by typing ```jupyter notebook``` into console. Open Telegraphs.ipynb and run all the cells.
5. Install missing packages by typing ```pip install *package name*``` into console. For example ```plotly```, ```itertools``` and ```pandas```.
